import { contants } from "."

// init
const inittialvalue = {
    listcineshow: [],
    description: {},
    informationbooking: {},
    timeandname: {
        movienamebooking: "",
        showtime: "",
        id: "",
    }
}

const reducer = (state = inittialvalue, { type, payload }) => {
    let newstate
    switch (type) {
        // PUSH_DESCRIPTION_LIST_MOVIE_SHOW
        case contants.PUSH_DESCRIPTION_LIST_MOVIE_SHOW:
            newstate = {
                ...state,
                listcineshow: payload.listcineshow,
                description: payload.description
            }
            break;
        // PUSH_INFORMATION_BOOKING
        case contants.PUSH_INFORMATION_BOOKING:
            newstate = {
                ...state,
                informationbooking: payload
            }
            break;
        // PUSH_MOVIE_NAME_BOOKING
        case contants.PUSH_MOVIE_NAME_BOOKING:
            newstate = {
                ...state,
                timeandname: payload
            }
            break;
        default:
            newstate = {
                ...state,
            }
            break;
    }
    return newstate
}

export default reducer