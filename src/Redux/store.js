import { applyMiddleware, combineReducers, createStore } from "redux";
import reduxSaga from "redux-saga";
import reducer from "./reducer";
import middleSaga from "./saga";

// create middle saga
const sagamiddle = reduxSaga()

//  state
const globalstate = combineReducers({
    reducer
})

// store
const store = createStore(
    globalstate,
    applyMiddleware(sagamiddle)
)

// run saga of us
sagamiddle.run(middleSaga)

export default store
