import { contants } from ".";

// call and push description and movie show
export const CALL_DESCRIPTION_LIST_MOVIE_SHOW = (payload) => {
    return {
        type: contants.CALL_DESCRIPTION_LIST_MOVIE_SHOW,
        payload
    }
}


export const PUSH_DESCRIPTION_LIST_MOVIE_SHOW = (payload) => {
    return {
        type: contants.PUSH_DESCRIPTION_LIST_MOVIE_SHOW,
        payload
    }
}

// CALL AND PUSH DESCRIPTION INFORMATION BOOKING
export const CALL_INFORMATION_BOOKING = (payload) => {
    return {
        type: contants.CALL_INFORMATION_BOOKING,
        payload
    }
}


export const PUSH_INFORMATION_BOOKING = (payload) => {
    return {
        type: contants.PUSH_INFORMATION_BOOKING,
        payload
    }
}

// PUSH MOVIE NAME BOOKING
export const PUSH_MOVIE_NAME_BOOKING = (payload) => {
    return {
        type: contants.PUSH_MOVIE_NAME_BOOKING,
        payload
    }
}

