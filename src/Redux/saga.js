import { call, put, takeEvery } from 'redux-saga/effects'
import { actions, contants } from '.'




// API descripttion and CineShow movie choose
async function callApiDescriptAndListCineShow(id) {
    // list cinima
    let listcine = await fetch(`https://vietcpq.name.vn/U2FsdGVkX19vV1e+G2Dt1h63IVituNJD+GdHSpis9+rOtKy+FbHJqg==/cinema/movie/${id}`)
    let listcineshow = await listcine.json()
    // dicription movie line API 4 (API 3 GET SLUG)
    let ojbdescription = await fetch(`https://vietcpq.name.vn/U2FsdGVkX19vV1e+G2Dt1h63IVituNJD+GdHSpis9+rOtKy+FbHJqg==/cinema/movieById/${id}`)
    let description = await ojbdescription.json()
    return {
        listcineshow,
        description
    }
}

//  CALL_INFORMATION_BOOKING
async function callApiInformationBooking() {
    let InforApi = await fetch(`https://teachingserver.org/U2FsdGVkX19vV1e+G2Dt1h63IVituNJD+GdHSpis9+rOtKy+FbHJqg==/cinema/booking/detail`)
    let informationbooking = await InforApi.json()
    return informationbooking
}

// midle handle data and call api
function* handleApiDescriptAndListCineShow({ payload }) {
    let ojb = yield call(callApiDescriptAndListCineShow, payload)
    yield put(actions.PUSH_DESCRIPTION_LIST_MOVIE_SHOW(ojb))
}


// CALL_INFORMATION_BOOKING
function* handleApiInforBooking({ payload }) {

    let InforApi = yield call(callApiInformationBooking, payload)
    yield put(actions.PUSH_INFORMATION_BOOKING(InforApi))
}

// midlesaga
function* middleSaga() {
    yield takeEvery(contants.CALL_DESCRIPTION_LIST_MOVIE_SHOW, handleApiDescriptAndListCineShow)
    yield takeEvery(contants.CALL_INFORMATION_BOOKING, handleApiInforBooking)
}

export default middleSaga