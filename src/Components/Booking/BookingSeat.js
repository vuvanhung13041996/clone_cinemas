import React, { useState } from 'react'
import './bookingseat.scss'
import Consession from './Consession.js'



export default function BookingSeat({ informationbooking, timeandname: { movienamebooking, showTime} }) {
    const [listchair, setListcheir] = useState([])
    const HandleChooseChair = (event) => {
        if (!event.target.setbackground) {
            // add style
            event.target.style.backgroundColor = "red"
            // set list chair
            setListcheir((previus) => {
                return [...previus, event.target.innerText]
            });
            // set true
            event.target.setbackground = true;
        } else {
            // add style
            event.target.style.backgroundColor = "burlywood"
            // set false
            event.target.setbackground = false
            setListcheir((previus) => {
                return previus.filter((element) => {
                    return element !== event.target.innerText;
                })
            });
        }
    }
    console.log(listchair);
    let areas = informationbooking?.seatPlan?.seatLayoutData?.areas
    return (<div className='container-bookingseat'>
        <h1>movie {movienamebooking}</h1>
        {areas?.map((element) => {
            let result = []
            for (let index = 0; index < element.rows.length; index++) {
                const obj = element.rows[index];
                let text = obj.physicalName;
                let nums = obj.seats;
                let row = []
                for (let i = 0; i < nums.length; i++) {
                    row.push(<span setbackground='false' className='bookingseat-row-seat' key={i} onClick={(event) => HandleChooseChair(event)}>{text + nums[i].id}</span>)
                }
                result.push(<div key={index} className='bookingseat-row'>{row}</div>)
            }
            console.log('result', result);
            // log result sẽ có 2 cái mảng lý do là vì nó chỉ là result của call back, không phải resul của cả hàm map
            return result
        })
        }
        <Consession listchair={listchair} movienamebooking={movienamebooking} showtime={showTime} />
    </div>)
}
