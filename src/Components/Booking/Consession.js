import React from 'react'
import './consession.scss'

export default function Consession({ movienamebooking, showtime, listchair }) {
    let numchair = listchair.length
    return (
        <div className='container-consession-choose'>
            <h2>{movienamebooking}</h2>
            <h2>Ghế
            {listchair.map((ele, index) => {
                return <span className='consession-choose' key={index}>{ele}</span>
            })}</h2>
            <h2>Suất chiếu {showtime}</h2>
            <h2>Số Tiền Cần Phải Thanh Toán {numchair*50}</h2>
            <button>TIẾP TỤC</button>
        </div>
    )
}
