import { connect } from 'react-redux'
import parse from 'html-react-parser';
import Select from './Select';
import './movieinfor.scss'


function MovieInfor({ description, listcineshow }) {
    return (
        <>
        
            {/* information */}
            <div className='container-description'>
                <a href={description.trailer} target="_blank"><img src={description.imagePortrait} alt='panner' /></a>
                <div className='container-description-right'>
                    <h1>{description.name}</h1>
                    <div>
                        <p>điểm:{description.point}/10</p>
                        <span>lượt Xem:{description.views}</span>
                    </div>
                    <p>Ngày Khởi Chiếu:{description.startdate?.slice(0, 10).split("-").reverse().join("-")}</p>
                    <p>Độ Tuổi Tối Thiểu:{description.age}</p>
                </div>
                <div className='container-description-bottom'>
                    {parse(description.description ?? "")}
                </div>
            </div>
            {/* Select */}
            <Select listcineshow={listcineshow} movieName={description.name}/>
        </>
    )
}

const MaspanStateToProps = (state) => {
    // DETRUCTORING GET KEY OBJECT
    const { description, listcineshow } = state.reducer
    return {
        description,
        listcineshow
    }
}

export default connect(MaspanStateToProps, undefined)(MovieInfor)