import React from 'react'
import { connect } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { actions } from '../Redux';
import './filtermovie.scss'




function FilterMovieShow({ filer, listcineshow, getInforBooking, pushMovieName, movieName }) {
    const nav = useNavigate()

    // select time
    const handleOnclickTimeShow = ({ showTime, id }) => {
        getInforBooking()
        // Booking Component
        pushMovieName(
            {
                movienamebooking: movieName,
                showTime,
                id
            })
        if (localStorage.getItem('User')) {
            nav('/Booking')
        } else {
            nav('/Login')
        }
    }

    return (<>
        {/* list cine show movie in city */}
        <div className='component-filter'>
            {listcineshow.filter((cine) => {
                return cine.cityId.includes(filer.cityId)
            }).filter((cine) => {
                return cine.id.includes(filer.cineId)
            })
                .map((cine, index) => {
                    return (
                        <div key={index} className='component-filter-child'>
                            {/* Name and Adress */}
                            <h2 className='component-filter-header'>{cine.name}</h2>
                            {/* type movie */}
                            <div className='component-filter-type-time'>
                                <p>{cine.dates[0].bundles[0].version} - {cine.dates[0].bundles[0].caption}</p>
                                <div>{cine.dates[0].bundles[0].sessions.map((session, index) => {
                                    return <span key={index} onClick={() => handleOnclickTimeShow(session)}>{session.showTime}</span>
                                })}
                                </div>
                            </div>
                        </div>)
                })}</div></>
    )
}
const MapDispatchToProps = (dispatch) => {
    return {
        getInforBooking() {
            dispatch(actions.CALL_INFORMATION_BOOKING())
        },
        pushMovieName(payload) {
            console.log(payload);
            dispatch(actions.PUSH_MOVIE_NAME_BOOKING(payload))
        }
    }
}

export default connect(undefined, MapDispatchToProps)(FilterMovieShow)