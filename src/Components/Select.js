import React, { useEffect, useState } from 'react'
import FilterMovieShow from './FilterMovieShow'
import './select.scss'

export default function Select(props) {
console.log(props.listcineshow);
    // state City
    const [listcity, setListCity] = useState(undefined)

    // change city
    const [cityId, setCityId] = useState('')

    // change cine
    const [cineId, setCineId] = useState('')

    // sent component
    let filter = {
        cityId,
        cineId
    }
    useEffect(() => {
        // push list state City 
        fetch('https://teachingserver.org/U2FsdGVkX19vV1e+G2Dt1h63IVituNJD+GdHSpis9+rOtKy+FbHJqg==/cinema/city')
            .then(response => response.json())
            .then(listcity => setListCity(listcity))
    }
        , [])
    // set day show movie
    const handleChangeCity = (event) => {
        setCityId(event.target.value);
    }

    // select cine
    const handleCine = (event) => {
        setCineId(event.target.value);
    }


    // select day
    const handleChangeDay = (event) => {

    }

    return (
        <>
            {/* fillter City */}
            <div className='container-select-component'>\
                {/* select city */}
                <select onChange={(event) => handleChangeCity(event)}>
                    <option value={''}>Cả Nước</option>
                    {
                        listcity?.map((city, index) => {
                            return <option key={index} value={city.id}>{city.name}</option>
                        })
                    }
                </select>

                {/* select day */}
                <select onChange={(event) => handleChangeDay(event)}>
                    {
                        props.listcineshow[0]?.dates.map((date, index) => {
                            return <option key={index} value={date.showDate}>{date.showDate}</option>
                        })
                    }
                </select>


                {/* select cine */}
                <select onChange={(event) => handleCine(event)}>
                    <option value={''}>Các Rạp</option>
                    {
                        // IIFE
                        (function () {
                            // varibal filter
                            let filter = props.listcineshow.filter(cine => {
                                return cine.cityId.includes(cityId)
                            })

                            let result

                            //  handle because filter can return array hollow, map can't loop and no return
                            if (filter.length > 0) {
                                result = filter.map((cine, index) => {
                                    return <option key={index} value={cine.id}>{cine.name}</option>
                                })
                            } else {
                                return <option>Thành Phố Này Hiện Không Chiếu Phim</option>
                            }
                            // return as function other
                            return result
                        })()
                    }
                </select>
            </div>
            {/* filter result */}
            <FilterMovieShow listcineshow={props.listcineshow} movieName={props.movieName} filer={filter} />
        </>
    )
}
