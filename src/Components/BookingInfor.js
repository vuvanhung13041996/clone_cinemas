import React from 'react'
import { connect } from 'react-redux';
import BookingSeat from './Booking/BookingSeat';
import './bookinginfor.scss'


function BookingInfor({ informationbooking, timeandname }) {
    return (
        <div className='container-bookinginfor'>
            <BookingSeat informationbooking={informationbooking} timeandname={timeandname} />
        </div>
    )
}

const MapStateToProps = (state) => {
    // DETRUCTORING GET KEY OBJECT
    const { informationbooking, timeandname } = state.reducer
    console.log(timeandname);
    return {
        informationbooking,
        timeandname
    }
}

export default connect(MapStateToProps, undefined)(BookingInfor)

