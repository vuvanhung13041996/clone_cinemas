import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { actions } from '../Redux'
import './nowsoon.scss'

function NowSoon(props) {
    // Content redner 
    const [moviecurent, setmoviecurent] = useState([])
    // STORE ALL MOVIE
    const [allmovie, setAllmovie] = useState({})
    // nav
    const nav = useNavigate()
    // DETRUCTORING
    const { movieCommingSoon, movieShowing } = allmovie
    // Fetch API NOW AND SOON
    useEffect(() => {
        fetch('https://vietcpq.name.vn/U2FsdGVkX19vV1e+G2Dt1h63IVituNJD+GdHSpis9+rOtKy+FbHJqg==/cinema/nowAndSoon')
            .then(response => response.json())
            .then(data => {
                setAllmovie(data);
                setmoviecurent(data.movieShowing)
            })
    }, [])

    // click in movie then call api
    const handleClickMovie = (id) => {
        props.CallApiDescripttionAndListcineShow(id)
        nav('MovieInfor')
    }

    // IU FIRST
    return (
        <div className='container-m-nowandsoon'>
            <div className='button-handle-group'>
                <button onClick={() => setmoviecurent(movieShowing)}>PHIM ĐANG CHIẾU</button>
                <button onClick={() => setmoviecurent(movieCommingSoon)}>PHIM SẮP CHIẾU</button>
            </div>

            {moviecurent.map((movie, index) => {
                return (
                    <div key={index} className='container-m-nowandsoon-children'>
                        <img src={movie.imageLandscape} alt='banner' />
                        <div className='container-m-nowandsoon-children_name' onClick={() => handleClickMovie(movie.id)}>
                            <h2>{movie.name}</h2>
                            <h2>{movie.subName ?? "Phim Hot Trong Nước"}</h2>
                        </div>
                        <div className='container-m-nowandsoon-children_hover' onClick={() => handleClickMovie(movie.id)}>
                            <div className='container-m-nowandsoon-children_hover-child'>ĐẶT VÉ</div>
                        </div>
                    </div>
                )
            })}
        </div>
    )
}

// Call Api Descripttion And ListcineShow
const MapDispatchToProps = (dispatch) => {
    return {
        // function to props
        CallApiDescripttionAndListcineShow(payload) {
            // sent action to saga
            dispatch(actions.CALL_DESCRIPTION_LIST_MOVIE_SHOW(payload))
        }
    }
}
export default connect(undefined, MapDispatchToProps)(NowSoon)