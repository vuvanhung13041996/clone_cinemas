import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import App from './App';
import MovieInfor from './Components/MovieInfor';
import BookingInfor from './Components/BookingInfor';
import store from './Redux/store';
import Login from './Components/Login';



const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Provider store={store}>
        <BrowserRouter>
            <Link to={'/'} />
            <Link to={'/MovieInfor'} />
            <Link to={'/Booking'} />
            <Link to={'/Login'} />
            <Routes>
                <Route path='/' element={<App />} />
                <Route path='/MovieInfor' element={<MovieInfor />} />
                <Route path='/Booking' element={<BookingInfor />} />
                <Route path='/Login' element={<Login />} />
            </Routes>
        </BrowserRouter>
    </Provider>
);
